import gleam/int
import gleam/list
import gleam/order
import gleam/string

fn parse_input(input: String) {
  input
  |> string.split(on: "\n")
  |> list.filter_map(int.parse)
}

pub fn pt_1(input: String) {
  parse_input(input)
  |> list.window_by_2
  |> list.filter_map(fn(t) {
    let #(a, b) = t
    case a < b {
      True -> Ok(a)
      False -> Error(a)
    }
  })
  |> list.length
}

pub fn pt_2(input: String) {
  parse_input(input)
  |> list.window(by: 3)
  |> list.window_by_2
  |> list.filter_map(fn(t) {
    let #(a, b) = t
    case int.sum(a) < int.sum(b) {
      True -> Ok(a)
      False -> Error(a)
    }
  })
  |> list.length
}
