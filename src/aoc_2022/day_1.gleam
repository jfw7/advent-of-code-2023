import gleam/int
import gleam/list
import gleam/string

pub fn pt_1(input: String) {
  input
  |> string.split(on: "\n\n")
  |> list.map(fn(inventory) {
    string.split(inventory, on: "\n")
    |> list.filter_map(int.parse)
    |> int.sum
  })
  |> list.fold(0, int.max)
}

pub fn pt_2(input: String) {
  input
  |> string.split(on: "\n\n")
  |> list.map(fn(inventory) {
    string.split(inventory, on: "\n")
    |> list.filter_map(int.parse)
    |> int.sum
  })
  |> list.sort(by: int.compare)
  |> list.reverse
  |> list.take(3)
  |> int.sum
}
