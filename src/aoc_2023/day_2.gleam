import gleam/int
import gleam/list
import gleam/option.{type Option, Some}
import gleam/regex
import gleam/result
import gleam/string

pub type Set {
  Set(blue: Int, red: Int, green: Int)
}

fn parse_input(input: String) {
  let assert Ok(re) = regex.from_string("Game (\\d+): (.*)")
  input
  |> string.split(on: "\n")
  |> list.map(fn(game) {
    let assert Ok(match) = list.first(regex.scan(with: re, content: game))
    let assert [Some(id), Some(sets)] = match.submatches
    let assert Ok(id) = int.parse(id)
    #(
      id,
      sets
      |> string.split(on: "; ")
      |> list.map(fn(set) {
        let counts =
          string.split(set, ", ")
          |> list.fold(
            from: Set(0, 0, 0),
            with: fn(set, count) {
              let [count, color] = string.split(count, " ")
              let assert Ok(count) = int.parse(count)
              case color {
                "red" -> Set(..set, red: count)
                "green" -> Set(..set, green: count)
                "blue" -> Set(..set, blue: count)
              }
            },
          )

        counts
      }),
    )
  })
}

pub fn pt_1(input: String) {
  parse_input(input)
  |> list.filter(fn(game) {
    let #(_, sets) = game
    list.map(sets, fn(set) { set.red })
    |> list.fold(from: 0, with: int.max) <= 12
  })
  |> list.filter(fn(game) {
    let #(_, sets) = game
    list.map(sets, fn(set) { set.green })
    |> list.fold(from: 0, with: int.max) <= 13
  })
  |> list.filter(fn(game) {
    let #(_, sets) = game
    list.map(sets, fn(set) { set.blue })
    |> list.fold(from: 0, with: int.max) <= 14
  })
  |> list.map(fn(game) {
    let #(id, _) = game
    id
  })
  |> int.sum
}

pub fn pt_2(input: String) {
  parse_input(input)
  |> list.map(fn(game) {
    let #(_, sets) = game
    let min_set =
      Set(
        red: list.map(sets, fn(set) { set.red })
        |> list.fold(from: 0, with: int.max),
        green: list.map(sets, fn(set) { set.green })
        |> list.fold(from: 0, with: int.max),
        blue: list.map(sets, fn(set) { set.blue })
        |> list.fold(from: 0, with: int.max),
      )
    min_set.red * min_set.blue * min_set.green
  })
  |> int.sum
}
