import gleam/int
import gleam/list
import gleam/string

fn parse_input(input) {
  input
  |> string.split(",")
}

fn get_hash(input: String) {
  input
  |> string.to_utf_codepoints()
  |> list.fold(
    0,
    fn(accumulator, current) {
      {
        accumulator
        |> int.add(
          current
          |> string.utf_codepoint_to_int(),
        )
        |> int.multiply(17)
      } % 256
    },
  )
}

pub fn pt_1(input: String) {
  input
  |> parse_input
  |> list.map(get_hash)
  |> int.sum
}

pub fn pt_2(input: String) {
  todo
}
