import gleam/io.{debug}
import gleam/int
import gleam/list
import gleam/order
import gleam/string
import gleam/result

pub type Card =
  Int

pub type Hand =
  List(Card)

pub type Bid =
  Int

pub type Type {
  FiveOfAKind
  FourOfAKind
  FullHouse
  ThreeOfAKind
  TwoPair
  OnePair
  HighCard
}

pub type HandWithBid {
  HandWithBid(hand: Hand, bid: Bid)
}

fn parse_input(input: String) {
  input
  |> string.split("\n")
  |> list.map(fn(line) {
    let assert [hand, bid] =
      line
      |> string.split(" ")
    let assert Ok(bid) = int.parse(bid)
    HandWithBid(
      hand: hand
      |> string.split("")
      |> list.map(fn(character) {
        case int.parse(character) {
          Ok(digit) -> digit
          Error(Nil) ->
            case character {
              "T" -> 10
              "J" -> 11
              "Q" -> 12
              "K" -> 13
              "A" -> 14
            }
        }
      }),
      bid: bid,
    )
  })
}

fn sort_hand_by_frequency(hand_with_bid: HandWithBid) {
  hand_with_bid.hand
  |> list.sort(fn(a, b) {
    case
      int.compare(
        {
          list.filter(hand_with_bid.hand, fn(el) { el == b })
          |> list.length()
        },
        {
          list.filter(hand_with_bid.hand, fn(el) { el == a })
          |> list.length()
        },
      )
    {
      order.Eq -> int.compare(b, a)
      default -> default
    }
  })
}

pub fn score_hand(hand: HandWithBid) {
  case sort_hand_by_frequency(hand) {
    [x1, x2, x3, x4, x5] if x1 == x2 && x2 == x3 && x3 == x4 && x4 == x5 -> 5
    [x1, x2, x3, x4, _] if x1 == x2 && x2 == x3 && x3 == x4 -> 4
    [x1, x2, x3, x4, x5] if x1 == x2 && x2 == x3 && x4 == x5 -> 3
    [x1, x2, x3, _, _] if x1 == x2 && x2 == x3 -> 2
    [x1, x2, x3, x4, _] if x1 == x2 && x3 == x4 -> 1
    [x1, x2, _, _, _] if x1 == x2 -> 0
    _ -> -1
  }
}

pub fn pt_1(input: String) {
  input
  |> parse_input
  |> list.sort(fn(a, b) {
    let assert Ok(comparison) = case int.compare(score_hand(a), score_hand(b)) {
      order.Eq ->
        list.map2(a.hand, b.hand, fn(x, y) { int.compare(x, y) })
        |> list.find(fn(x) { x != order.Eq })
      default -> Ok(default)
    }
    comparison
  })
  |> list.index_map(fn(i, hand_with_bid) { hand_with_bid.bid * { i + 1 } })
  |> int.sum
}

pub fn pt_2(input: String) {
  todo
}
