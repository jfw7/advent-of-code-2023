import gleam/int
import gleam/list
import gleam/string

pub fn pt_1(input: String) {
  let [times, distances] =
    input
    |> string.split("\n")
    |> list.map(fn(line) {
      string.split(line, " ")
      |> list.filter_map(int.parse)
    })

  list.zip(times, distances)
  |> list.map(fn(t) {
    let #(time, distance) = t
    list.range(0, time)
    |> list.map(fn(t) { int.multiply(t, time - t) })
    |> list.filter(fn(d) { d > distance })
    |> list.length
  })
  |> int.product
}

pub fn pt_2(input: String) {
  let [times, distances] =
    input
    |> string.split("\n")
    |> list.map(fn(line) {
      string.split(line, " ")
      |> list.filter_map(int.parse)
      |> list.map(int.to_string)
      |> string.join("")
      |> int.parse
    })

  let assert Ok(times) = times
  let assert Ok(distances) = distances

  list.zip([times], [distances])
  |> list.map(fn(t) {
    let #(time, distance) = t
    list.range(0, time)
    |> list.map(fn(t) { int.multiply(t, time - t) })
    |> list.filter(fn(d) { d > distance })
    |> list.length
  })
  |> int.product
}
