import gleam/function
import gleam/int
import gleam/list
import gleam/string

fn parse_input(input: String) {
  input
  |> string.split("\n")
  |> list.map(fn(line) {
    string.split(line, " ")
    |> list.filter_map(int.parse)
  })
}

fn generate_sequence_differences(sequences: List(List(Int))) {
  let [first, ..] = sequences
  case list.unique(first) {
    [0] -> sequences
    _ ->
      generate_sequence_differences([
        first
        |> list.window_by_2
        |> list.map(fn(t) { t.1 - t.0 }),
        ..sequences
      ])
  }
}

pub fn pt_1(input: String) {
  input
  |> parse_input
  |> list.map(fn(sequence) {
    generate_sequence_differences([sequence])
    |> list.filter_map(list.last)
    |> list.reduce(fn(a, b) { a + b })
  })
  |> list.filter_map(function.identity)
  |> int.sum
}

pub fn pt_2(input: String) {
  input
  |> parse_input
  |> list.map(fn(sequence) {
    generate_sequence_differences([sequence])
    |> list.filter_map(list.first)
    |> list.reduce(fn(a, b) { b - a })
  })
  |> list.filter_map(function.identity)
  |> int.sum
}
