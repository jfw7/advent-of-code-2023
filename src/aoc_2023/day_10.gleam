import gleam/io.{debug}
import gleam/int
import gleam/list
import gleam/map.{type Map}
import gleam/option.{type Option, None, Some}
import gleam/set.{type Set}
import gleam/string
import gleam/result

pub type Direction {
  North
  East
  South
  West
}

pub type Coordinate {
  Coordinate(x: Int, y: Int)
}

pub type TileMap {
  TileMap(start: Option(Coordinate), tiles: Map(Coordinate, Set(Direction)))
}

fn parse_input(input: String) {
  input
  |> string.split("\n")
  |> list.index_fold(
    from: TileMap(start: None, tiles: map.new()),
    with: fn(tile_map: TileMap, line: String, y: Int) {
      list.index_fold(
        over: string.split(line, ""),
        from: tile_map,
        with: fn(tile_map: TileMap, char: String, x: Int) {
          case char {
            "|" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([North, South]),
                ),
              )
            "-" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([East, West]),
                ),
              )
            "L" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([North, East]),
                ),
              )
            "J" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([North, West]),
                ),
              )
            "7" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([South, West]),
                ),
              )
            "F" ->
              TileMap(
                ..tile_map,
                tiles: map.insert(
                  tile_map.tiles,
                  Coordinate(x, y),
                  set.from_list([South, East]),
                ),
              )
            "S" -> TileMap(..tile_map, start: Some(Coordinate(x, y)))
            _ -> tile_map
          }
        },
      )
    },
  )
}

fn get_neighbor(coordinate: Coordinate, direction: Direction) {
  case direction {
    North -> Coordinate(..coordinate, y: coordinate.y - 1)
    South -> Coordinate(..coordinate, y: coordinate.y + 1)
    East -> Coordinate(..coordinate, x: coordinate.x + 1)
    West -> Coordinate(..coordinate, x: coordinate.x - 1)
  }
}

fn get_distances_of_neighbors(
  point: Coordinate,
  tiles: Map(Coordinate, Set(Direction)),
  distances: Map(Coordinate, Int),
) {
  let assert Ok(distance) =
    distances
    |> map.get(point)

  let neighbors =
    tiles
    |> map.filter(fn(coordinate: Coordinate, directions: Set(Direction)) {
      set.filter(
        directions,
        fn(direction) { get_neighbor(coordinate, direction) == point },
      )
      |> set.size() > 0
    })

  distances
  |> map.merge(
    neighbors
    |> map.filter(fn(coordinate, _) {
      map.has_key(distances, coordinate) == False
    })
    |> map.map_values(fn(coordinate, _) {
      let assert Ok(distance) =
        get_distances_of_neighbors(
          coordinate,
          tiles,
          distances
          |> map.merge(
            neighbors
            |> map.map_values(fn(_, _) { debug(distance + 1) }),
          ),
        )
        |> map.get(coordinate)
      distance
    }),
  )
}

pub fn pt_1(input: String) {
  let tile_map = parse_input(input)
  let assert Some(start) = tile_map.start

  let tiles =
    tile_map.tiles
    |> map.filter(fn(coordinate: Coordinate, directions: Set(Direction)) {
      directions
      |> set.filter(fn(direction: Direction) {
        let neighbor = get_neighbor(coordinate, direction)
        case direction {
          North ->
            result.unwrap(map.get(tile_map.tiles, neighbor), set.new())
            |> set.contains(South) || { neighbor == start }
          South ->
            result.unwrap(map.get(tile_map.tiles, neighbor), set.new())
            |> set.contains(North) || { neighbor == start }
          East ->
            result.unwrap(map.get(tile_map.tiles, neighbor), set.new())
            |> set.contains(West) || { neighbor == start }
          West ->
            result.unwrap(map.get(tile_map.tiles, neighbor), set.new())
            |> set.contains(East) || { neighbor == start }
        }
      })
      |> set.size() == 2
    })

  let assert Ok(#(neighbor, _)) =
    tile_map.tiles
    |> map.filter(fn(coordinate: Coordinate, directions: Set(Direction)) {
      set.filter(
        directions,
        fn(direction) { get_neighbor(coordinate, direction) == start },
      )
      |> set.size() > 0
    })
    |> map.to_list
    |> list.first()

  get_distances_of_neighbors(
    neighbor,
    tiles,
    map.from_list([#(start, 0), #(neighbor, 1)]),
  )
}

pub fn pt_2(input: String) {
  todo
}
