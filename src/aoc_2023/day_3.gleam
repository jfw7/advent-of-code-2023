import gleam/int
import gleam/list
import gleam/queue.{Queue}
import gleam/result
import gleam/string
import gleam/string_builder
import gleam/io.{debug}
import gleam/regex

pub type Point {
  Point(x: Int, y: Int)
}

pub type Result {
  Result(number: Int, position: Point, length: Int)
}

pub type Neighborhood {
  Neighborhood(number: Int, neighborhood: String)
}

fn get_neighborhood(grid: List(List(String)), result: Result) {
  Neighborhood(
    number: result.number,
    neighborhood: list.range(
      result.position.x - 1,
      result.position.x + result.length,
    )
    |> list.flat_map(fn(x) {
      list.range(result.position.y - 1, result.position.y + 1)
      |> list.map(fn(y) {
        let line = result.unwrap(list.at(grid, y), [])
        result.unwrap(list.at(line, x), "")
      })
    })
    |> string.join(""),
  )
}

fn get_numbers_with_positions(
  line: List(String),
  x: Int,
  y: Int,
  results: Queue(Result),
) {
  case line {
    [char, ..rest_of_line] -> {
      let results = case result.is_ok(int.parse(char)) {
        True -> {
          let assert Ok(digit) = int.parse(char)
          let #(result, results) =
            result.unwrap(
              queue.pop_back(results),
              #(Result(0, Point(x, y), 0), queue.new()),
            )
          queue.push_back(
            results,
            Result(
              position: case result.length {
                0 -> Point(x, y)
                _ -> result.position
              },
              number: 10 * result.number + digit,
              length: result.length + 1,
            ),
          )
        }
        False -> queue.push_back(results, Result(0, Point(x, y), 0))
      }
      get_numbers_with_positions(rest_of_line, x + 1, y, results)
    }
    _ -> queue.to_list(results)
  }
  |> list.filter(fn(result: Result) { result.length > 0 })
}

fn parse_input(input: String) {
  input
  |> string.split("\n")
  |> list.map(fn(line) { string.split(line, on: "") })
}

pub fn pt_1(input: String) {
  let grid = parse_input(input)
  grid
  |> list.index_map(fn(line_number, line) {
    get_numbers_with_positions(line, 0, line_number, queue.new())
    |> list.map(fn(result) { get_neighborhood(grid, result) })
  })
  |> list.flatten
  |> list.filter(fn(n) {
    let assert Ok(re) = regex.from_string("[^\\d.]")
    regex.check(re, n.neighborhood)
  })
  |> list.map(fn(n) { n.number })
  |> int.sum
}

fn replace_asterisks(input: String, codepoint: Int) {
  case string.contains(input, "*") {
    True -> {
      let assert Ok(#(pre, post)) = string.split_once(input, "*")
      let assert Ok(char) = string.utf_codepoint(codepoint)
      pre <> string.from_utf_codepoints([char]) <> replace_asterisks(
        post,
        codepoint + 1,
      )
    }
    False -> input
  }
}

pub fn pt_2(input: String) {
  let min_codepoint = 256
  let input = replace_asterisks(input, min_codepoint)
  let grid = parse_input(input)
  let results =
    grid
    |> list.index_map(fn(line_number, line) {
      get_numbers_with_positions(line, 0, line_number, queue.new())
      |> list.map(fn(result) { get_neighborhood(grid, result) })
    })
    |> list.flatten
  let max_codepoint =
    list.fold(
      string.split(input, ""),
      0,
      fn(b, a) {
        let [codepoint] = string.to_utf_codepoints(a)
        int.max(string.utf_codepoint_to_int(codepoint), b)
      },
    )
  list.range(min_codepoint, max_codepoint)
  |> list.filter_map(fn(codepoint) {
    let assert Ok(codepoint) = string.utf_codepoint(codepoint)
    let char = string.from_utf_codepoints([codepoint])
    let hits =
      list.filter(
        results,
        fn(result) { string.contains(result.neighborhood, char) },
      )
    case list.length(hits) {
      2 -> Ok(hits)
      _ -> Error([])
    }
  })
  |> list.map(fn(hits) {
    let [a, b] = hits
    a.number * b.number
  })
  |> int.sum
}
