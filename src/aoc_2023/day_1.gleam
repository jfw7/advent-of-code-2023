import gleam/int
import gleam/list
import gleam/result
import gleam/string

fn parse_input(input: String) {
  input
  |> string.split(on: "\n")
}

pub fn pt_1(input: String) {
  parse_input(input)
  |> list.map(fn(line: String) {
    string.split(line, on: "")
    |> list.filter_map(int.parse)
  })
  |> list.map(fn(digits) {
    let assert Ok(first) = list.first(digits)
    let assert Ok(last) = list.last(digits)
    10 * first + last
  })
  |> int.sum
}

fn parse_digits(characters: List(String), digits digits: List(Int)) {
  case characters {
    ["1", ..rest] | ["o", "n", "e", ..rest] ->
      parse_digits(["n", "e", ..rest], [1, ..digits])
    ["2", ..rest] | ["t", "w", "o", ..rest] ->
      parse_digits(["w", "o", ..rest], [2, ..digits])
    ["3", ..rest] | ["t", "h", "r", "e", "e", ..rest] ->
      parse_digits(["h", "r", "e", "e", ..rest], [3, ..digits])
    ["4", ..rest] | ["f", "o", "u", "r", ..rest] ->
      parse_digits(["o", "u", "r", ..rest], [4, ..digits])
    ["5", ..rest] | ["f", "i", "v", "e", ..rest] ->
      parse_digits(["i", "v", "e", ..rest], [5, ..digits])
    ["6", ..rest] | ["s", "i", "x", ..rest] ->
      parse_digits(["i", "x", ..rest], [6, ..digits])
    ["7", ..rest] | ["s", "e", "v", "e", "n", ..rest] ->
      parse_digits(["e", "v", "e", "n", ..rest], [7, ..digits])
    ["8", ..rest] | ["e", "i", "g", "h", "t", ..rest] ->
      parse_digits(["i", "g", "h", "t", ..rest], [8, ..digits])
    ["9", ..rest] | ["n", "i", "n", "e", ..rest] ->
      parse_digits(["i", "n", "e", ..rest], [9, ..digits])
    [_, ..rest] -> parse_digits(rest, digits)
    _ -> digits
  }
}

pub fn pt_2(input: String) {
  parse_input(input)
  |> list.map(fn(line: String) {
    string.split(line, on: "")
    |> parse_digits(digits: [])
  })
  |> list.map(fn(digits) {
    let assert Ok(first) = list.first(digits)
    let assert Ok(last) = list.last(digits)
    10 * last + first
  })
  |> int.sum
}
