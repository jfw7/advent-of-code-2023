import gleam/float
import gleam/int
import gleam/io.{debug}
import gleam/list
import gleam/option.{type Option, Some}
import gleam/result
import gleam/set.{type Set}
import gleam/string
import gleam/regex

pub type Card {
  Card(
    number: String,
    winning_numbers: Set(String),
    numbers_you_have: Set(String),
  )
}

fn parse_input(input: String) {
  let assert Ok(re) =
    regex.from_string("Card\\s*(\\d*):\\s*(.*)\\s*\\|\\s*(.*)\\s*")
  input
  |> string.split("\n")
  |> list.map(fn(card) {
    let assert Ok(match) = list.first(regex.scan(re, card))
    let assert [Some(number), Some(winning_numbers), Some(numbers_you_have)] =
      match.submatches
    let assert Ok(re) = regex.from_string("\\s+")
    Card(
      number: number,
      winning_numbers: set.from_list(regex.split(
        re,
        string.trim(winning_numbers),
      )),
      numbers_you_have: set.from_list(regex.split(
        re,
        string.trim(numbers_you_have),
      )),
    )
  })
}

pub fn pt_1(input: String) {
  parse_input(input)
  |> list.map(fn(card) {
    set.intersection(card.numbers_you_have, card.winning_numbers)
    |> set.size()
  })
  |> list.filter_map(fn(n) {
    case n {
      0 -> Error(Nil)
      _ -> int.power(2, int.to_float(n))
    }
  })
  |> list.map(float.truncate)
  |> int.sum
  |> int.divide(2)
}

pub fn pt_2(input: String) {
  todo
}
