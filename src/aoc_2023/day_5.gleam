import gleam/int
import gleam/list
import gleam/string
import gleam/io.{debug}

// import gleam/result

pub type MapLine {
  MapLine(
    destination_range_start: Int,
    source_range_start: Int,
    range_length: Int,
  )
}

pub fn parse_input(input: String) {
  let [seeds, ..maps] =
    input
    |> string.split("\n\n")
  let [_, ..rest] = string.split(seeds, " ")
  let seeds =
    rest
    |> list.filter_map(int.parse)
  let maps =
    maps
    |> list.map(fn(map) {
      let [_, ..rest] = string.split(map, "\n")
      rest
      |> list.map(fn(map) {
        let [destination_range_start, source_range_start, range_length] =
          string.split(map, " ")
          |> list.filter_map(int.parse)
        MapLine(destination_range_start, source_range_start, range_length)
      })
    })
  #(seeds, maps)
}

pub fn pt_1(input: String) {
  let #(seeds, maps) =
    input
    |> parse_input
  maps
  |> list.fold(
    from: seeds,
    with: fn(sources, map_lines) {
      map_lines
      |> list.fold(
        from: sources,
        with: fn(sources, map_line) {
          sources
          |> list.map(fn(source) {
            case
              source >= map_line.source_range_start && source < map_line.source_range_start + map_line.range_length
            {
              True ->
                int.negate(
                  source - map_line.source_range_start + map_line.destination_range_start,
                )
              False -> source
            }
          })
        },
      )
      |> list.map(int.absolute_value)
    },
  )
  |> list.reduce(int.min)
}

pub fn pt_2(input: String) {
  todo
}
