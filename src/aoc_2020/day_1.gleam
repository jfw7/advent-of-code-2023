import gleam/int
import gleam/list
import gleam/string

fn parse_input(input: String) {
  input
  |> string.split(on: "\n")
  |> list.filter_map(int.parse)
}

pub fn pt_1(input: String) {
  parse_input(input)
  |> list.combination_pairs()
  |> list.find_map(fn(t) {
    case t.0 + t.1 {
      2020 -> Ok(t.0 * t.1)
      _ -> Error(0)
    }
  })
}

pub fn pt_2(input: String) {
  parse_input(input)
  |> list.combinations(by: 3)
  |> list.find_map(fn(l) {
    case int.sum(l) {
      2020 -> Ok(list.fold(l, from: 1, with: fn(a, x) { a * x }))
      _ -> Error(0)
    }
  })
}
